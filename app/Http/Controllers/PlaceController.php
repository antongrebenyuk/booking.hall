<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Place;
use App\models\Scheme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlaceController extends Controller
{
    public function savePlaces(Request $request)
    {
        $chairCount = $request->input('chairCount');

        for ($index = 1; $index <= $chairCount; $index++) {
            $places = new Place;
            $places->place_number = $index;
            $places->scheme_id = $request->input('schemeId');
            $places->save();
        }
    }

    public function ToBooking(Request $request)
    {
        $schemeId = $request->input('schemeId');
        $placeNumber = $request->input('placeNumber');

        $getPlaceId = Place::where('scheme_id', '=', $schemeId)->where('place_number', '=', $placeNumber)->get('id');
        foreach ($getPlaceId as $booking) {
            $placeId = $booking->id;
        }

        $getUserId = Event::where('scheme', '=', $schemeId)->get('user_id');
        foreach ($getUserId as $userIds) {
            $userId = $userIds->user_id;
        }

        if (Place::where('scheme_id', '=', $schemeId)->where('guest_name', '=', Auth::user()->name)->count() > 0
            && $userId != Auth::user()->id) {
            $result = 'false';
        } else {
            $toBooking = Place::find($placeId);
            $toBooking->guest_name = $request->input('guestName');
            $toBooking->save();
            $result = 'true';
        }

        return $result;
    }

    public function getBookingPlaces(Request $request)
    {
        $id = $request->input('id');
        $bookingPlaces = Place::where('scheme_id', '=', $id)->whereNotNull('guest_name')->get(['place_number', 'guest_name']);

        return $bookingPlaces;
    }

    public function cancelBooking(Request $request)
    {
        Place::where('scheme_id', '=', $request->input('schemeId'))
            ->where('place_number', '=', $request->input('placeNumber'))
            ->update(['guest_name' => null]);
    }
}
