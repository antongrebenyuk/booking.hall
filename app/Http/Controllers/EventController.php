<?php

namespace App\Http\Controllers;

use App\Models\Place;
use App\Models\Scheme;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::where('user_id', '=', Auth::user()->id)->whereNotNull('scheme')->get();

        foreach ($events as $key => $event) {
            $events[$key]['totalCountPlaces'] = Place::where('scheme_id', '=', $event->scheme)->count();
            $events[$key]['bookingCountPlaces'] = Place::where('scheme_id', '=', $event->scheme)->whereNotNull('guest_name')->count();
        };

        return $events;
    }

    public function store(Request $request)
    {
        Event::create(array_merge($request->only(['name', 'city', 'address', 'date', 'time', 'is_anonymously',
            'is_closed', 'code']), ['user_id' => Auth::user()->id]));
    }

    public function saveScheme(Request $request)
    {
        $scheme = Scheme::create([
            'json_info' => $request->input('json'),
            'last_chair_id' => $request->input('lastChairId'),
            'last_table_id' => $request->input('lastTableId')
        ]);

        $idsEvent = Event::orderBy('id', 'desc')->limit(1)->get('id');
        foreach ($idsEvent as $idEvent) {
            $eventId = $idEvent->id;
        }

        $eventScheme = Event::find($eventId);
        $eventScheme->scheme = $scheme->id;
        $eventScheme->save();

        return $scheme->id;
    }

    public function update(Request $request)
    {
        Event::where('id', $request->input('id'))->update($request->only(['name', 'city', 'address', 'date', 'time',
            'is_anonymously', 'is_closed', 'code']));
    }

    public function destroy(Request $request)
    {
        Event::destroy($request->input('id'));
    }

    public function getParams(Request $request)
    {
        return Event::find($request->input('id'));
    }

    public function getScheme(Request $request)
    {
        $schemeId = $request->input('schemeId');
        $scheme = Scheme::find($schemeId);
        $event = Event::where('scheme', '=', $schemeId)->firstOrFail();

        return json_encode([
            'scheme' => $scheme->json_info,
            'lastChair' => $scheme->last_chair_id,
            'lastTable' => $scheme->last_table_id,
            'user_id' => $event->user_id
        ]);
    }

    public function updateScheme(Request $request)
    {
        $scheme = Scheme::find($request->input('schemeId'));
        $scheme->json_info = $request->input('json');
        $scheme->last_chair_id = $request->input('lastChairId');
        $scheme->last_table_id = $request->input('lastTableId');
        $scheme->save();
    }
}
