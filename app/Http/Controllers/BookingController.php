<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function getParams(Request $request)
    {
        return Event::where('scheme', '=', $request->input('id'))->firstOrFail();
    }

    public function getAuthorEventId(Request $request)
    {
        return Event::where('scheme', '=', $request->input('id'))->firstOrFail();
    }

    public function getDateEvent(Request $request)
    {
        $eventParams = Event::where('scheme', '=', $request->input('id'))->firstOrFail();
        $secondsEvens = strtotime($eventParams->date . ' ' . $eventParams->time);
        $showEvent = ($secondsEvens > time()) ? true : false;

        return compact(['showEvent']);
    }
}