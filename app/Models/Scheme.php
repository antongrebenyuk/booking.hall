<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{
    protected $fillable = ['json_info', 'last_chair_id', 'last_table_id'];

    public function phone()
    {
        return $this->hasOne(Event::class);
    }

    protected $casts = [
      'json_info' => 'json'
    ];
}
