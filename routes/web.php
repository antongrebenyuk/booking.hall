<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('/save-params', 'EventController@store');
Route::post('/save-scheme', 'EventController@saveScheme');
Route::post('/update-scheme', 'EventController@updateScheme');
Route::get('/test', 'EventController@test');
Route::get('/get-institutions', 'EventController@index');
Route::get('/get-params', 'EventController@getParams');
Route::get('/get-scheme', 'EventController@getScheme');
Route::patch('/edit-params', 'EventController@update');
Route::delete('/destroy', 'EventController@destroy');

Route::post('/save-places', 'PlaceController@savePlaces');
Route::post('/to-booking', 'PlaceController@ToBooking');
Route::post('/cancel-booking', 'PlaceController@cancelBooking');
Route::get('/get-booking-places', 'PlaceController@getBookingPlaces');

Route::get('/get-params-booking', 'BookingController@getParams');
Route::get('/get-author-event-id', 'BookingController@getAuthorEventId');
Route::get('/get-date-event', 'BookingController@getDateEvent');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


