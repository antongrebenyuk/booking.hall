window.Vue = require('vue');

// компонент гл.стр
const mainPage = Vue.component('main-page', require('../components/MainPage.vue').default);
// компонент ЛК(мои схемы)
const mySchemes = Vue.component('my-schemes', require('../components/MySchemes.vue').default);
// Компонент добавления заведения( 1 - Параметры заведения)
const institutionParams = Vue.component('params', require('../components/InstitutionParams.vue').default);
// Компонент добавления заведения ( 2 - Выбор редактора)
const chooseEditor = Vue.component('choose-editor', require('../components/ChooseEditor.vue').default);
// Компонент добавления заведения ( 3 - Редактор схемы)
const constructor = Vue.component('constructor', require('../components/Constructor.vue').default);
// Компонент созданной схемы с возможностью бронирования
const booking = Vue.component('booking', require('../components/Booking.vue').default);
// Компонент авторизации
const authorization = Vue.component('authorization', require('../components/Authorization.vue').default);
// Компонент редактирования заведения (параметры)
const editParams = Vue.component('edit', require('../components/EditParams.vue').default);
// Компонент редактирования заведения (схема)
const editScheme = Vue.component('edit-scheme', require('../components/EditScheme.vue').default);
// Компонент ошибки
const error404 = Vue.component('error404', require('../components/Error404.vue').default);

export const routes = [
    {
        path: '/',  // путь гл.стр
        component: mainPage,
        //проверка авторизации юзера
        beforeEnter: (to, from, next) => {
            axios.get('/auth/google/callback').then(response => {
                //токен есть, с главной страницы отправляем на мои схемы
                next('/my-schemes');
            }).catch(error => {
                //токена нет, оставляем на главной
                next();
            })
        }
    },
    {
        path: '/my-schemes', // путь мои схемы (личный кабинет)
        component: mySchemes,
        name: "MySchemes",
        beforeEnter: (to, from, next) => {
            guard(to, from, next);
        }
    },
    {
      path: '/params', // путь добабвления заведения ( 1 - Параметры)
      component: institutionParams,
        //проверка авторизации юзера
      beforeEnter: (to, from, next) => {
           guard(to, from, next);
      }
    },
    {
      path: '/choose-editor', // путь добабвления заведения ( 2 - Выбор редактора)
      component: chooseEditor,
        //проверка авторизации юзера
      beforeEnter: (to, from, next) => {
          guard(to, from, next);
      }
    },
    {
        path: '/constructor', // путь добабвления заведения ( 3 - Конструктор схемы)
        component: constructor,
        beforeEnter: (to, from, next) => {
            guard(to, from, next);
        }
    },
    {
        path: '/booking/:id', // путь готовой схемы с возможность бронирования, где id - id схемы заведения в БД
        component: booking,
        name: 'booking',
        //проверка авторизации юзера
        beforeEnter: (to, from, next) => {
            guard(to, from, next);
        }
    },
    {
        path: '/authorization', // путь авторизации
        component: authorization,
        //проверка авторизации юзера
        beforeEnter: (to, from, next) => {
            axios.get('/auth/google/callback').then(response => {
                //токен есть, с главной страницы отправляем на мои схемы
                next('/my-schemes');
            }).catch(error => {
                //токена нет, оставляем на главной
                next();
            })
        }
    },
    {
        path: '/edit/:id', // редактирование заведения
        component: editParams,
        name: 'edit',
        props: true,
        //проверка авторизации юзера
        beforeEnter: (to, from, next) => {
            guard(to, from, next);
        }
    },
    {
        path: '/edit-scheme/:id', // редактирование заведения
        component: editScheme,
        name: 'editScheme',
        props: true,
        //проверка авторизации юзера
        beforeEnter: (to, from, next) => {
            guard(to, from, next);
        }
    },
    {
        path: '*',
        component: error404
    }
];

const guard = function(to, from, next) {
    // проверяем токен авторизации
    axios.get('/auth/google/callback').then(response => {
        // токен есть, остаемся на этой странице
        next();
    }).catch(error => {
        // токена нет, делаем редирект
        next("/authorization")
    })
};



//закрываем главную страницу и страницу авторизации авторизированным пользователям
const userCheck = function(to, from, next) {
    // проверяем токен авторизации

}
