@extends('layouts.app')

<script>
    @auth
        window.App = <?php echo json_encode([
            'userId'      => Auth::user()->id,
            'userName'    => Auth::user()->name
        ]); ?>
    @endauth
</script>

@section('content')
    <router-view></router-view>
@endsection
