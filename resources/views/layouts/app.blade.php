<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/clipboard.js/latest/clipboard.min.js"></script>
    <title>Booking.hall</title>
</head>
<body>
<div id="app">
    <header>
        <div class="container_">
            <div class="logo_bg">
                <div class="logo_box">
                    <router-link to="/">
                        <span class="booking_logo">Booking.</span>
                        <span class="hall_logo">hall</span>
                    </router-link>
                    @guest
                        <span class="lk_name"></span>
                    @else
                        <span class="lk">
                            <button onclick="myFunction()" class="dropbtnmenu relative" style='background: url("/storage/img/menu.png")no-repeat center; background-size: cover;'></button>
                        </span>
                        <span class="lk_name" onclick="myFunction()">Привет, {{ Auth::user()->name }}!</span>
                            <span class="lk">
                                <span class="dropdown">
                                <button onclick="myFunction()" class="dropbtn" style='background: url("{{ Auth::user()->user_avatar }}")no-repeat center; background-size: cover;'></button>
                                <div id="myDropdown" class="dropdown-content">
                                    <div class="pointer"></div>
                                    <router-link to="/my-schemes">Мои мероприятия</router-link>
                                    <router-link to="/params">Добавить мероприятие</router-link>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                            Выход
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </span>
                        </span>
                    @endguest

                </div>
            </div>
        </div>
    </header>
    <main>
        @yield('content')
    </main>
    <footer>
        <div class="container_">
            <div class="logo_bg_footer">
                <div class="logo_box_footer">
                    <router-link to="/">
                        <span class="booking_logo">Booking.</span>
                        <span class="hall_logo">hall</span>
                    </router-link>
                    <div class="footer_year">© 2019</div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

<script>
    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches('.dropbtnmenu')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>
