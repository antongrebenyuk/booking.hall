<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('Название заведения');
            $table->integer('user_id')->comment('ID юзера, который добавил заведение');
            $table->integer('scheme_id')->comment('ID схемы заведения');
            $table->string('city')->comment('Город, в котором находится заведение');
            $table->integer('count_guests')->comment('Количество гостей');
            $table->integer('price_per_person')->comment('Цена за бронирование с человека');
            $table->string('image')->comment('Фотография заведения');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
    }
}
