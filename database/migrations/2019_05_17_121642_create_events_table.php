<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name')->comment('Название мероприятия');
            $table->string('city')->comment('Город проведения');
            $table->string('address')->comment('Адрес проведения');
            $table->date('date')->comment('Дата проведения');
            $table->time('time')->comment('Время проведения');
            $table->boolean('is_anonymously')->comment('Показывать/НЕ показывать имена гостей');
            $table->boolean('is_closed')->comment('Закрытое(код мероприятия)/Открытое');
            $table->integer('code')->nullable();
            $table->longText('scheme')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
